import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'profile',
    loadChildren: () => import('./profile/profile.module').then( m => m.ProfilePageModule)
  },
  {
    path: 'modelview',
    loadChildren: () => import('./modelview/modelview.module').then( m => m.ModelviewPageModule)
  },
  {
    path: 'storyui',
    loadChildren: () => import('./storyui/storyui.module').then( m => m.StoryuiPageModule)
  },
  {
    path: 'slide-background',
    loadChildren: () => import('./slide-background/slide-background.module').then( m => m.SlideBackgroundPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
