import { Component, Input, OnInit ,ViewChild,ElementRef} from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Gesture, GestureController } from '@ionic/angular';

@Component({
  selector: 'app-model',
  templateUrl: './model.component.html',
  styleUrls: ['./model.component.scss'],
})
export class ModelComponent implements OnInit {

  constructor(private modelctrl : ModalController,private gestureCtrl: GestureController) { }

  @ViewChild('main') divMain: ElementRef;

  @Input() name : string;
  @Input() education : string;

  ngOnInit() {

  }

  async ngAfterViewInit() {
    const longPress = this.gestureCtrl.create({
      el: this.divMain.nativeElement,
      threshold: 0,
      gestureName: 'tap-press',
      onStart: ev => {
        console.log('move start!');
      },
      onEnd: ev => {
        this.dismiss()
      }
    }, true); // Passing true will run the gesture callback inside of NgZone!

    longPress.enable(true);
  }
  dismiss(){
    this.modelctrl.dismiss({
      "fromModel" : "ModelDismissed by Hitendra"
    })
  }

}
