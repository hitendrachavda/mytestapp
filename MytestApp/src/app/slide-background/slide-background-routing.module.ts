import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SlideBackgroundPage } from './slide-background.page';

const routes: Routes = [
  {
    path: '',
    component: SlideBackgroundPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SlideBackgroundPageRoutingModule {}
