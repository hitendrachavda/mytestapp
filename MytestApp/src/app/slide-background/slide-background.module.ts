import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SlideBackgroundPageRoutingModule } from './slide-background-routing.module';

import { SlideBackgroundPage } from './slide-background.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SlideBackgroundPageRoutingModule
  ],
  declarations: [SlideBackgroundPage]
})
export class SlideBackgroundPageModule {}
