import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { StoryuiPageRoutingModule } from './storyui-routing.module';

import { StoryuiPage } from './storyui.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    StoryuiPageRoutingModule
  ],
  declarations: [StoryuiPage]
})
export class StoryuiPageModule {}
