import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StoryuiPage } from './storyui.page';

const routes: Routes = [
  {
    path: '',
    component: StoryuiPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StoryuiPageRoutingModule {}
