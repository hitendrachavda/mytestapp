import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ModelComponent } from '../components/model/model.component';

@Component({
  selector: 'app-modelview',
  templateUrl: './modelview.page.html',
  styleUrls: ['./modelview.page.scss'],
})
export class ModelviewPage implements OnInit {

  constructor(private modelCtrl : ModalController) { }

  fromModel : any;
  hide : boolean = true;

  ngOnInit() {
  }

  async openModel(){
    this.hide = !this.hide
    const model = await this.modelCtrl.create({
      component: ModelComponent,
      componentProps : {
        "name" : "hitendra",
        "education" : "MCA"
      },
      cssClass : "my-modal-component-css",
      showBackdrop:true,
      backdropDismiss:false,
      animated : false,
    })
    model.onDidDismiss().then((data:any )=>{
      this.fromModel =  data.data.fromModel ;
      this.hide = !this.hide
    })
    return await model.present();
  }

  hideDiv(){
    console.log("touched")
    this.hide = !this.hide
  }

}
