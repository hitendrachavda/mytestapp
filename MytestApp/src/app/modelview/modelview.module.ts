import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModelviewPageRoutingModule } from './modelview-routing.module';

import { ModelviewPage } from './modelview.page';
import { ModelComponent } from '../components/model/model.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ModelviewPageRoutingModule
  ],
  declarations: [
    ModelviewPage,
  ]
})
export class ModelviewPageModule {}
