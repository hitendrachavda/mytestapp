import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ModelviewPage } from './modelview.page';

const routes: Routes = [
  {
    path: '',
    component: ModelviewPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModelviewPageRoutingModule {}
